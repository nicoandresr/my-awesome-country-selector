import React from "react";
import "./style.css";
import Layout from './components/layout';
import Headline from './components/headline';
import RegionPicker from './components/region-picker';
import useSWR from 'swr';
import { url, fetcher } from './api';
import { DataContext } from './state';

export default function App() {
  const { data } = useSWR(url, fetcher);

  return (
    <DataContext.Provider value={data}>
      <Layout>
        <Headline>Select Region</Headline>

        <RegionPicker />
      </Layout>
    </DataContext.Provider>
  );
}
