import fetch from 'unfetch';

export const url = `https://restcountries.com/v3.1/all`;
export const fetcher = (url) => fetch(url).then((r) => r.json());
