import * as React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const propsTypes = {
  textt: PropTypes.string
}

function Headline(props) {
  return (
    <div className={`headline`}>
      <h1 className={`headline__title`}>      
        {props.children}
      </h1>
    </div>
  );
}

Headline.propsTypes = propsTypes;

export default Headline;