import * as React from 'react';
import PropTypes from 'prop-types';
import { DataContext } from '../../state';
import capitalize from 'js-capitalize';
import './style.css';

const propTypes = {
  show: PropTypes.bool,
  picked: PropTypes.string,
  cta: PropTypes.func,
};

function CountryList(props) {
  const { show, picked, cta } = props;
  const [list, setList] = React.useState([]);
  const data = React.useContext(DataContext);
  const [countries, setCountries] = React.useState([]);

  const onClick = countryId => () => {
    if (list.includes(countryId)) {
      setList([...list.filter(id => id !== countryId)]);
      return;
    }

    setList([...list, countryId]);
  }

  React.useEffect(() => {
    if (picked === 'none') {
      setCountries([]);
      setList([]);
      return;
    }

    const region = capitalize(picked);
    setTimeout(() => {
      setCountries(data.filter(d => d.region === region));
    }, 1000);

  }, [picked]);

  return (
    show && (
      <>
        <section className={`country-list`}>
          <h2 className={`country-list__title`}>Select countries</h2>
          {countries.map(({ cca3: id, name: { common: name } }) => {
            const sel = list.includes(id) ? '--sel' : '';
            return (
              <>
                <span
                  id={`name_${id}`}
                  key={`name_${id}`}
                  title={name}
                  className={`country-list__name${sel}`}
                  onClick={onClick(id)}
                >
                  {name.slice(0, 20)}
                </span>

                <div
                  id={`check_${id}`}
                  key={`check_${id}`}
                  onClick={onClick(id)}
                  className={`country-list__check${sel}`}
                />
              </>
            )
          })}
        </section>
        {cta({ isValid: list.length > 0 })}
      </>
    )
  );
}

CountryList.propTypes = propTypes;

export default CountryList;
