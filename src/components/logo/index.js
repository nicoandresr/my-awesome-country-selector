import * as React from 'react';

import './style.css';

import ThemeManager from '../theme-manager';

function Logo() {
  const [state, setState] = React.useState('close');
  const toogleMenu = () =>
    setState(prev => prev === 'close' ? 'open' : 'close');

  return (
    <div className="logo">
      <div className={`logo__menu--${state}`}>
        <ThemeManager />
      </div>

      <img
        className="logo__img"
        src="https://ut0p14.s3.us-east-2.amazonaws.com/utopia-platform.webp"
        alt="utopia open platform"
      />

      <div
        className={`logo__lash--${state}`}
        onClick={toogleMenu}
      >
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="340.547 110.902 5.906 5.196" width="5.906pt" height="5.196pt" className={`logo__polygon--${state}`}>
          <polygon points="346.453,116.098,340.547,116.098,343.5,110.902" />
        </svg>
      </div>
    </div>
  );
}

export default Logo;