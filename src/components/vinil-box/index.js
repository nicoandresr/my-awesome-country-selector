import * as React from 'react';
import PropTypes from 'prop-types';
import CallToActions from '../call-to-actions';
import CountryList from '../country-list';
import './style.css';

const propsTypes = {
  id: PropTypes.string,
  picked: PropTypes.string,
  title: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.node,
  onClick: PropTypes.func
};

function VinilBox(props) {
  const { title, className, children, onPick, id, picked } = props;
  const selected = id === picked;
  function clickHandler() {
    onPick(id);
  }

  return (
    <>
      <div className={`vinil-box ${className}`} onClick={clickHandler} id={id}>
        {children}
        <span className={`vinil-box__name`}>{title}</span>
      </div>

      <CountryList
        picked={picked}
        show={selected}
        cta={({ isValid }) => (
          <CallToActions
            onBack={clickHandler}
            show={selected}
            disableNext={!isValid}
            onNext={() =>
              alert(`thank you to use my demo App
                I hope that you liked it!! ^_^
                Powered by NicoandresR`
              )
            }
          />
        )}
      />
    </>
  );
}

VinilBox.propTypes = propsTypes;

export default VinilBox;
