import * as React from 'react';
import gsap from 'gsap';

import { coor } from './data';

export function usePick(sectionEl) {
  const [selected, setSelected] = React.useState('none');
  const updateSelected = value => {
    setSelected(prev => {
      if (prev === value || value == 'none') {
        sectionEl.current.classList.remove('region-picker--sel');
        setTimeout(
          () => 
          gsap.to(`#${value}`, { x: 0, y: 0 }),
          500
        );
        return 'none';
      }

      setTimeout(
        () => sectionEl.current.classList.add('region-picker--sel'),
        1000
      );
      gsap.to(`#${value}`, coor[value]);
      return value;
    });
  };

  return [selected, updateSelected];
}

export const useClassNames = (selected, value) => {
  let classNames = `region-picker__img`;
  classNames += ` region-picker__img--${value}`;
  if (selected === value) {
    classNames += ` region-picker__img--${value}-sel`;
  } else if (selected !== 'none') {
    classNames += ` region-picker__img--hid`;
  }

  return classNames;
};
