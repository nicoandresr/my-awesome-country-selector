import * as React from 'react';
import VinilBox from '../vinil-box';
import { useClassNames, usePick } from './hooks.js';
import regions from './data.js';
import './style.css';

function RegionPicker() {
  const sectionEl = React.useRef();
  const [pick, setPick] = usePick(sectionEl);

  return (
    <section ref={sectionEl} className={`region-picker`}>
      {regions.map(({ name, value }) => (
        <VinilBox
          key={value}
          title={name}
          id={value}
          picked={pick}
          onPick={setPick}
          className={useClassNames(pick, value)}
        />
      ))}
    </section>
  );
}

export default RegionPicker;