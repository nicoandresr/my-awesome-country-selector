export default [
  {
    name: 'Americas',
    value: 'americas'
  },
  {
    name: 'Europe',
    value: 'europe'
  },
  {
    name: 'Africa',
    value: 'africa'
  },
  {
    name: 'Asia',
    value: 'asia'
  },
  {
    name: 'Oceania',
    value: 'oceania'
  },
];

export const coor = {
  americas: { x: 15 },
  europe: { x: 15, y: -155 },
  africa: { x: 15, y: -311 },
  asia: { x: -135, y: -53 },
  oceania: { x: -135, y: -258 },
};