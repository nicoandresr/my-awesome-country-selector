import * as React from 'react';
import './style.css';

function ThemeManager() {

  /*
  * controls the theme css variables, by a clousure in javascript,
  * here not is necesary a react state, just a small clousure for
  * change the theme variable
  */
  function toggleTheme() {
    let theme = 'dark';
    return () => {
      theme = theme === 'dark' ? 'light' : 'dark';
      document.documentElement.setAttribute('data-theme', theme);
    }
  }

  return (
    <button
      type="button"
      className="theme-btn"
      onClick={toggleTheme()}
    />
  );
}

export default ThemeManager;