import * as React from 'react';
import PropTypes from 'prop-types';

import './style.css';

import Logo from '../logo';

const propsTypes = {
  children: PropTypes.node
}

function Layout(props) {
  return (
    <main className="main-layout">
      <Logo />
      
      {props.children}
    </main>
  );
}

Layout.propsTypes = propsTypes;

export default Layout;