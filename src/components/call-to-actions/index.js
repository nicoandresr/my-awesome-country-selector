import * as React from 'react';
import PropTypes from 'prop-types';
import './style.css';

const propTypes = {
  onBack: PropTypes.func,
  onNext: PropTypes.func,
  show: PropTypes.bool,
  disableNext: PropTypes.bool,
};

function CallToActions(props) {
  const { onBack, onNext, show, disableNext } = props;
  const disableNextMod = disableNext ? '--disabled' : '';

  return show && (
    <section
      className={`cta`}
    >
      <button
        type="button"
        onClick={onBack}
        className={`cta__back`}
      >
        Back
      </button>

      <button
        type="button"
        onClick={onNext}
        className={`cta__next${disableNextMod}`}
        disabled={disableNext}
      >
        Next
      </button>
    </section>
  );
}

CallToActions.propTypes = propTypes;

export default CallToActions;